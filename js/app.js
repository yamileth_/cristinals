// Inicializar la biblioteca de animaciones WOW.js
new WOW().init();

// Animación del título principal
var title = document.querySelector('h1');
title.classList.add('animate__animated');

// Animación de la profesión
var profession = document.querySelector('#profesion');
profession.classList.add('animate__animated');

// Animación de la imagen de perfil
var profileImage = document.querySelector('.img-fluid');
profileImage.classList.add('animate__animated', 'animate__zoomIn');

// Animación de las habilidades
var skillListItems = document.querySelectorAll('.skill-list li');
skillListItems.forEach(function(item) {
    item.classList.add('animate__animated', 'animate__fadeInLeft');
});

// Animación del contenido de "Sobre mí"
var aboutContent = document.querySelector('.animated.fadeInRight');
aboutContent.classList.add('animate__animated', 'animate__fadeInRight');

// Animación de la lista de información de contacto
var contactInfoItems = document.querySelectorAll('.contact-info li');
contactInfoItems.forEach(function(item) {
    item.classList.add('animate__animated', 'animate__fadeInRight');
});